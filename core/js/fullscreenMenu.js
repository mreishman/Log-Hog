function toggleFullScreenMenu(force = false)
{
	fullScreenMenuClickCount++;
	dirForAjaxSend = "";
	closeLogPopup();
	if(document.getElementById("fullScreenMenu").style.display === "none")
	{
		document.getElementById("menu").style.zIndex = "4";
		loadImgFromData("mainMenuImage");
		document.getElementById("fullScreenMenu").style.display = "block";
		onScrollShowFixedMiniBar(arrayOfScrollHeaderUpdate);
		if($("#ThemesLink") && $("#ThemesLink").hasClass("selected"))
		{
			toggleThemesIframeSource(true);
		}
		else if($("#mainMenuNotifications") && $("#mainMenuNotifications").hasClass("selected"))
		{
			updateNotificationStuff();
		}
		else if($("#mainFullScreenMenu li.selected").length === 0)
		{
			//first load of full screen menu, switch to defined initial page
			if(fullScreenMenuDefaultPage !== "none")
			{
				if(fullScreenMenuDefaultPage === "about")
				{
					toggleAbout();
				}
				else if(fullScreenMenuDefaultPage === "history")
				{
					toggleHistory();
				}
				else if(fullScreenMenuDefaultPage === "notifications")
				{
					toggleNotifications();
				}
				else if(fullScreenMenuDefaultPage === "settings")
				{
					toggleSettings();
				}
				else if(fullScreenMenuDefaultPage === "themes")
				{
					toggleThemes();
				}
				else if(fullScreenMenuDefaultPage === "update" && ($.inArray("updaterEnabled", JSON.parse(moduleDisabledList)) < 0))
				{
					toggleUpdateMenu();
				}
				else if(fullScreenMenuDefaultPage === "watchlist")
				{
					toggleWatchListMenu();
				}
			}
		}
		resizeFullScreenMenu();
		var fullScreenMenuClickCountCurrent = fullScreenMenuClickCount;
		setTimeout(function() {
			togglePollSpeedDown(fullScreenMenuClickCountCurrent);
		}, 1000 * fullScreenMenuPollSwitchDelay);

	}
	else
	{
		updateDocumentTitle('Index');
		if(!force && !globalForcePageNavigate)
		{
			if(!(goToPageCheck("toggleFullScreenMenu(true)")))
			{
				return false;
			}
		}
		toggleThemesIframeSource(false);
		$( "#fullScreenMenuWatchList" ).off( "mousemove" );
		globalForcePageNavigate = false;
		document.getElementById("menu").style.zIndex = "20";
		hideIframeStuff();
		hideMainStuff();
		arrayOfScrollHeaderUpdate = [];
		document.getElementById("fullScreenMenu").style.display = "none";
		togglePollSpeedUp();
	}
}

function toggleSettingSection(data, force = false)
{
	if(!force && !globalForcePageNavigate)
	{
		if(!(goToPageCheck("toggleSettingSection("+JSON.stringify(data)+", true)")))
		{
			return false;
		}
	}
	globalForcePageNavigate = false;
	hideSettingsStuff();
	endSettingsPollTimer();
	document.getElementById(data.id).style.display = "block";
	$("#"+data.id+"Menu").addClass("selected");
	arrayOfScrollHeaderUpdate = [];
	if(data.formId != "false")
	{
		arrayOfDataSettings = [data.formId];
		arrayOfScrollHeaderUpdate = [data.formId];
	}
	onScrollShowFixedMiniBar(arrayOfScrollHeaderUpdate);
	resizeFullScreenMenu();
	startSettingsPollTimer();
}

function toggleUpdateMenu(force = false)
{
	if(!force && !globalForcePageNavigate)
	{
		if(!(goToPageCheck("toggleUpdateMenu(true)")))
		{
			return false;
		}
	}
	updateDocumentTitle("Update");
	globalForcePageNavigate = false;
	loadImgFromData("updateImg");
	hideMainStuff();
	hideSidebar();
	document.getElementById("fullScreenMenuUpdate").style.display = "block";
	$("#mainMenuUpdate").addClass("selected");
	arrayOfScrollHeaderUpdate = [];
	onScrollShowFixedMiniBar(arrayOfScrollHeaderUpdate);
	resizeFullScreenMenu();
}

function toggleHistory(force = false)
{
	if(!force && !globalForcePageNavigate)
	{
		if(!(goToPageCheck("toggleHistory(true)")))
		{
			return false;
		}
	}
	updateDocumentTitle('History');
	globalForcePageNavigate = false;
	hideMainStuff();
	toggleFullScreenMenuMainContent();
	document.getElementById("historySubMenu").style.display = "block";
	$("#mainMenuHistory").addClass("selected");
	arrayOfScrollHeaderUpdate = [];
	onScrollShowFixedMiniBar(arrayOfScrollHeaderUpdate);
	toggleTmpSaveHistory();
}

function toggleTmpSaveHistory()
{
	hideHistoryStuff();
	document.getElementById("fullScreenMenuHistory").style.display = "block";
	$("#tempSaveHistory").addClass("selected");
	getListOfTmpHistoryLogs();
	resizeFullScreenMenu();
}

function toggleArchiveHistory()
{
	hideHistoryStuff();
	document.getElementById("fullScreenMenuArchive").style.display = "block";
	$("#archiveHistory").addClass("selected");
	getListOfArchiveLogs();
	resizeFullScreenMenu();
}

function toggleSettings(force = false)
{
	if(!force && !globalForcePageNavigate)
	{
		if(!(goToPageCheck("toggleSettings(true)")))
		{
			return false;
		}
	}
	updateDocumentTitle("Settings");
	globalForcePageNavigate = false;
	hideMainStuff();
	toggleFullScreenMenuMainContent();
	document.getElementById("settingsSubMenu").style.display = "block";
	$("#mainMenuSettings").addClass("selected");
	arrayOfScrollHeaderUpdate = [];
	onScrollShowFixedMiniBar(arrayOfScrollHeaderUpdate);
	resizeFullScreenMenu();
}

function toggleAbout(force = false)
{
	if(!force && !globalForcePageNavigate)
	{
		if(!(goToPageCheck("toggleAbout(true)")))
		{
			return false;
		}
	}
	updateDocumentTitle('About');
	globalForcePageNavigate = false;
	hideMainStuff();
	toggleFullScreenMenuMainContent();
	document.getElementById("aboutSubMenu").style.display = "block";
	$("#mainMenuAbout").addClass("selected");
	toggleAboutLogHog();
}

function toggleThemes(force = false)
{
	if(!force && !globalForcePageNavigate)
	{
		if(!(goToPageCheck("toggleThemes(true)")))
		{
			return false;
		}
	}
	updateDocumentTitle("Themes");
	globalForcePageNavigate = false;
	hideMainStuff();
	hideSidebar();
	toggleFullScreenMenuMainContent();
	$("#ThemesLink").addClass("selected");
	toggleMainThemes();
}

function toggleMainThemes(force = false)
{
	if(!force)
	{
		if(!(goToPageCheck("toggleMainThemes(true)")))
		{
			return false;
		}
	}
	hideThemeStuff();
	endSettingsPollTimer();
	toggleThemesIframeSource(true);
	document.getElementById("fullScreenMenuTheme").style.display = "block";
	arrayOfScrollHeaderUpdate = ["themeSpan"];
	onScrollShowFixedMiniBar(arrayOfScrollHeaderUpdate);
	resizeFullScreenMenu();
}

function toggleThemesIframeSource(showOrHide)
{
	var arrayOfIframesForThemes = document.getElementsByClassName("iframeThemes");
	var lengthOfIframeThemes = arrayOfIframesForThemes.length;
	for(var counterOfIframeThemes = 0; counterOfIframeThemes < lengthOfIframeThemes; counterOfIframeThemes++)
	{
		let newSrc = "core/html/iframe.html";
		let oldSrc = arrayOfIframesForThemes[counterOfIframeThemes].getAttribute("src");

		if(showOrHide)
		{
			newSrc = arrayOfIframesForThemes[counterOfIframeThemes].getAttribute("data-src");
		}
		else if (!oldSrc)
		{
			continue;
		}
		

		if(!oldSrc.endsWith(newSrc))
		{
			arrayOfIframesForThemes[counterOfIframeThemes].setAttribute("src",newSrc);
		}
	}
}

function toggleFullScreenMenuMainContent()
{
	var mainContentFullScreenMenuLeft = "402px";
	var mainContentFullScreenMenuTop = "46px";
	if( window.innerWidth < breakPointTwo || sideBarOnlyIcons === "breakpointtwo")
	{
		mainContentFullScreenMenuLeft = "52px";
		mainContentFullScreenMenuTop = "82px";
	}
	else if(sideBarOnlyIcons === "breakpointone" || window.innerWidth < breakPointOne)
	{
		mainContentFullScreenMenuLeft = "252px";
	}

	if(document.getElementById("mainContentFullScreenMenu").style.left !== mainContentFullScreenMenuLeft)
	{
		document.getElementById("mainContentFullScreenMenu").style.left = mainContentFullScreenMenuLeft;
	}

	if(document.getElementById("mainContentFullScreenMenu").style.top !== mainContentFullScreenMenuTop)
	{
		document.getElementById("mainContentFullScreenMenu").style.top = mainContentFullScreenMenuTop;
	}
}

function toggleAboutLogHog()
{
	hideAboutStuff();
	document.getElementById("fullScreenMenuAbout").style.display = "block";
	$("#aboutSubMenuAbout").addClass("selected");
	arrayOfScrollHeaderUpdate = ["aboutSpanAbout","aboutSpanInfo","aboutSpanGithub"];
	onScrollShowFixedMiniBar(arrayOfScrollHeaderUpdate);
	resizeFullScreenMenu();
}

function toggleChangeLog()
{
	loadImgFromData("whatsNewImage");
	hideAboutStuff();
	document.getElementById("fullScreenMenuChangeLog").style.display = "block";
	$("#aboutSubMenuChangelog").addClass("selected");
	arrayOfScrollHeaderUpdate = ["fullScreenMenuChangeLog"];
	onScrollShowFixedMiniBar(arrayOfScrollHeaderUpdate);
	resizeFullScreenMenu();
}

function toggleWatchListMenu(force = false)
{
	if(!force && !globalForcePageNavigate)
	{
		if(!(goToPageCheck("toggleWatchListMenu(true)")))
		{
			return false;
		}
	}
	updateDocumentTitle("Watchlsit");
	globalForcePageNavigate = true;
	if(typeof loadWatchList !== "function")
	{
		let scriptToLoad = onLoadJsFiles["watchlist"];
		script(scriptToLoad["name"] + "?v=" + scriptToLoad["ver"]);
	}
	else
	{
		resetProgressBarWatchList();
	}
	$(".uniqueClassForAppendSettingsMainWatchNew").html("");
	$("#loadingSpan").show();
	toggleFullScreenMenuMainContent();
	loadImgFromData("watchlistImg");
	hideMainStuff();
	arrayOfDataSettings = ["settingsMainWatch"];
	document.getElementById("fullScreenMenuWatchList").style.display = "block";
	document.getElementById("watchListSubMenu").style.display = "block";
	$("#watchListMenu").addClass("selected");
	arrayOfScrollHeaderUpdate = ["settingsMainWatch"];
	onScrollShowFixedMiniBar(arrayOfScrollHeaderUpdate);
	$(".settingsMainWatchSaveChangesButton").css("display","none");
	document.getElementById("watchListSubMenu").style.display = "none";
	if(typeof loadWatchList !== "function")
	{
		setTimeout(function() {
			timerForWatchlist = setInterval(tryLoadWatch, 100);
		}, 250);
	}
	else
	{
		setTimeout(function() {
			timerForWatchlist = setInterval(tryLoadWatch, 100);
		}, 25);
	}
	resizeFullScreenMenu();
}

function tryLoadWatch()
{
	if(typeof loadWatchList === "function")
	{
		clearInterval(timerForWatchlist);
		loadWatchList();
	}
}

function startSettingsPollTimer()
{
	timerForSettings = setInterval(checkIfChanges, 100);
}

function checkIfChanges()
{
	if(checkForChangesArray(arrayOfDataSettings))
	{
		return true;
	}
	return false;
}

function goToPageCheck(functionName)
{
	try
	{
		var goToPage = !checkIfChanges();
		popupSettingsArrayCheck();
		if(!(goToPage || ("saveSettings" in popupSettingsArray  && popupSettingsArray.saveSettings == "false")))
		{
			displaySavePromptPopupIndex(functionName);
			return false;
		}
		return true;
	}
	catch(e)
	{
		console.log(e);
	}
}

function endSettingsPollTimer()
{
	arrayOfDataSettings = [];
	clearInterval(timerForSettings);
}

function hideSidebar()
{
	sideBarVisible = false;
	var mainContentFullScreenMenuLeft = "201px";
	if(sideBarOnlyIcons === "breakpointone" || window.innerWidth < breakPointOne || sideBarOnlyIcons === "breakpointtwo")
	{
		mainContentFullScreenMenuLeft = "52px";
	}

	if(document.getElementById("mainContentFullScreenMenu").style.left !== mainContentFullScreenMenuLeft)
	{
		document.getElementById("mainContentFullScreenMenu").style.left = mainContentFullScreenMenuLeft;
	}
	if(document.getElementById("mainContentFullScreenMenu").style.top !== "46px")
	{
		document.getElementById("mainContentFullScreenMenu").style.top = "46px";
	}

}

function hideWatchListStuff()
{
	document.getElementById("fullScreenMenuWatchList").style.display = "none";
	document.getElementById("watchListSubMenu").style.display = "none";
}

function hideUpdateStuff()
{
	document.getElementById("fullScreenMenuUpdate").style.display = "none";
}

function hideSettingsStuff()
{
	$(".fullScreenMenuSettingsFormDiv").hide();
	$(".fullScreenMenuSettingsFormTitle").removeClass("selected");
}

function hideAboutStuff()
{
	document.getElementById("fullScreenMenuAbout").style.display = "none";
	$("#aboutSubMenuAbout").removeClass("selected");
	document.getElementById("fullScreenMenuChangeLog").style.display = "none";
	$("#aboutSubMenuChangelog").removeClass("selected");
}

function hideHistoryStuff()
{
	document.getElementById("fullScreenMenuHistory").style.display = "none";
	$("#tempSaveHistory").removeClass("selected");
	document.getElementById("fullScreenMenuArchive").style.display = "none";
	$("#archiveHistory").removeClass("selected");
}

function hideNotificationStuff()
{
	document.getElementById("notifications").style.display = "none";
	document.getElementById("notificationsEmpty").style.display = "none";
}

function hideThemeStuff()
{
	toggleThemesIframeSource(false);
	document.getElementById("fullScreenMenuTheme").style.display = "none";
}

function hideIframeStuff()
{
	document.getElementById("fullScreenMenuIFrame").style.display = "none";
	let newSrc = "core/html/iframe.html";
	let oldSrc = $("#iframeFullScreen").prop("src");
	if (!oldSrc)
	{
		return;
	}
	if(!oldSrc.endsWith(newSrc))
	{
		$("#iframeFullScreen").prop("src", newSrc);
	}
}

function hideMainStuff()
{
	endSettingsPollTimer();

	if($("#mainMenuSettings").hasClass("selected"))
	{
		hideSettingsStuff();
		document.getElementById("settingsSubMenu").style.display = "none";
		$("#mainMenuSettings").removeClass("selected");
	}

	if($("#mainMenuNotifications").hasClass("selected"))
	{
		hideNotificationStuff();
		$("#mainMenuNotifications").removeClass("selected");
		sideBarVisible = true;
	}

	if($("#mainMenuAbout").hasClass("selected"))
	{
		document.getElementById("aboutSubMenu").style.display = "none";
		hideAboutStuff();
		$("#mainMenuAbout").removeClass("selected");
	}

	if($("#mainMenuHistory").hasClass("selected"))
	{
		document.getElementById("historySubMenu").style.display = "none";
		hideHistoryStuff();
		$("#mainMenuHistory").removeClass("selected");
	}

	if($("#mainMenuUpdate").hasClass("selected"))
	{
		hideUpdateStuff();
		$("#mainMenuUpdate").removeClass("selected");
		sideBarVisible = true;
	}

	if($("#watchListMenu").hasClass("selected"))
	{
		hideWatchListStuff();
		$("#watchListMenu").removeClass("selected");
	}

	if($("#ThemesLink") && $("#ThemesLink").hasClass("selected"))
	{
		hideThemeStuff();
		$("#ThemesLink").removeClass("selected");
		sideBarVisible = true;
	}
}

function onScrollShowFixedMiniBar(idsOfForms)
{
	if(!document.getElementById("fixedPositionMiniMenu"))
	{
		return;
	}
	if(idsOfForms.length < 1)
	{
		$("#fixedPositionMiniMenu").html("");
		if(document.getElementById("fixedPositionMiniMenu").style.display !== "none")
		{
			document.getElementById("fixedPositionMiniMenu").style.display = "none";
		}
		return;
	}
	var widthOfMainMenu = document.getElementById("mainContentFullScreenMenu").getBoundingClientRect().width;
	if (document.getElementById("fixedPositionMiniMenu").style.width !==  widthOfMainMenu)
	{
		document.getElementById("fixedPositionMiniMenu").style.width = widthOfMainMenu;
	}
	var dis = false;
	for (var i = idsOfForms.length - 1; i >= 0; i--)
	{
		var currentPos = document.getElementById(idsOfForms[i]).getBoundingClientRect().top;
		var topCheck = 52;
		if(window.innerWidth < breakPointTwo || sideBarOnlyIcons === "breakpointtwo")
		{
			topCheck = 88;
		}
		if(currentPos < topCheck)
		{
			$("#fixedPositionMiniMenu").html($("#"+idsOfForms[i]+" .settingsHeader").html());
			if(document.getElementById("fixedPositionMiniMenu").style.display === "none")
			{
				document.getElementById("fixedPositionMiniMenu").style.display = "block";
			}
			var fixedPositionMiniMenuTop = "52px";
			if(window.innerWidth < breakPointTwo || sideBarOnlyIcons === "breakpointtwo")
			{
				fixedPositionMiniMenuTop = "88px";
			}
			if(document.getElementById("fixedPositionMiniMenu").style.top !== fixedPositionMiniMenuTop)
			{
				document.getElementById("fixedPositionMiniMenu").style.top = fixedPositionMiniMenuTop;
			}
			dis = true;
			break;
		}
	}
	if(!dis)
	{
		$("#fixedPositionMiniMenu").html("");
		if(document.getElementById("fixedPositionMiniMenu").style.display !== "none")
		{
			document.getElementById("fixedPositionMiniMenu").style.display = "none";
		}
	}
}

function resizeFullScreenMenu()
{
	try
	{
		var targetWidth = window.innerWidth;
		var mainContentFullScreenMenuLeft = "402";
		if(!sideBarVisible)
		{
			mainContentFullScreenMenuLeft = "201";
		}
		var mainContentFullScreenMenuTop = "52px";
		if(sideBarOnlyIcons === "breakpointone" || targetWidth < breakPointOne || sideBarOnlyIcons === "breakpointtwo")
		{
			$(".fullScreenMenuText").hide();
			if(document.getElementById("mainFullScreenMenu").getBoundingClientRect().width !== 52) //1px border included here
			{
				document.getElementById("mainFullScreenMenu").style.width = "51px";
				$(".settingsUlSub").css("left", "52px");
			}
			mainContentFullScreenMenuLeft = "252";
			if(!sideBarVisible)
			{
				mainContentFullScreenMenuLeft = "52";
			}
		}
		else
		{
			$(".fullScreenMenuText").not(".hiddenMenu").show();
			if(document.getElementById("mainFullScreenMenu").getBoundingClientRect().width !== 201) //1px border included here
			{
				document.getElementById("mainFullScreenMenu").style.width = "200px";
				$(".settingsUlSub").css("left", "201px");
			}
		}

		if(targetWidth < breakPointTwo || sideBarOnlyIcons === "breakpointtwo")
		{
			mainContentFullScreenMenuLeft = "52";
			if(sideBarVisible)
			{
				mainContentFullScreenMenuTop = "158px";
			}
			$(".settingsUlSub").css("width","auto").css("bottom","auto").css("right","0").addClass("addBorderBottom").removeClass("addBorderRight").css("height","105px");
			$(".settingsUlSub li").not(".subMenuToggle, .hiddenMenu").css("display","inline-block");
			$("#settingsSubMenu .menuTitle").not(".menuBreak , .fullScreenNotificationTitle").css("display","block");
			$("#settingsSubMenu .menuTitle").not(".menuBreak , .fullScreenNotificationTitle , .subMenuTitle").hide();
		}
		else
		{
			$(".settingsUlSub").css("width","200px").css("bottom","0").css("right","auto").removeClass("addBorderBottom").addClass("addBorderRight").css("height","auto");
			$(".settingsUlSub li").not(".subMenuToggle, .hiddenMenu").css("display","block");
			$("#settingsSubMenu .menuTitle").not(".fullScreenMenuText, .hiddenMenu").css("display","block");
		}

		if(document.getElementById("mainContentFullScreenMenu").style.left !== mainContentFullScreenMenuLeft+"px")
		{
			document.getElementById("mainContentFullScreenMenu").style.left = mainContentFullScreenMenuLeft+"px";
		}
		if(document.getElementById("mainContentFullScreenMenu").style.top !== mainContentFullScreenMenuTop)
		{
			document.getElementById("mainContentFullScreenMenu").style.top = mainContentFullScreenMenuTop;
		}
		if(document.getElementById("notificationHolder").style.width !== (window.innerWidth - mainContentFullScreenMenuLeft)+"px")
		{
			document.getElementById("notificationHolder").style.width = (window.innerWidth - mainContentFullScreenMenuLeft)+"px";
		}
	}
	catch(e)
	{
		console.log(e);
	}
}

function togglePollSpeedDown(currentClick)
{
	if(userPaused || pausePollCurrentSession || currentClick !== fullScreenMenuClickCount)
	{
		return;
	}
	clearPollTimer();
	if(fullScreenMenuPollSwitchType === "BGrate")
	{
		pollingRateBackup = pollingRate;
		pollingRate = backgroundPollingRate;
		startPollTimer();
	}
}

function togglePollSpeedUp()
{
	if(userPaused || pausePollCurrentSession)
	{
		return;
	}
	clearPollTimer();
	if(pollingRateBackup !== 0)
	{
		pollingRate = pollingRateBackup;
	}
	pollingRateBackup = 0;
	startPollTimer();
}